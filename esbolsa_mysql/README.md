Kaltiot MySQL Cookbook
==============

Installs MySQL client and server (in case it is stated) and configures it.

IMPORTANT: MySQL installation is done manually because I have not found yet the way to make the installation work with OpsWorks.
As soon as an alternative is found we should use it, because this cookbook does not consider reinstallations or things like that.


Requirements
------------
### Cookbooks
No dependencies

### Platforms
This cookbook has been tested for 

 - Centos 6.6
 - Amazon Linux 2014.09

Attributes
----------
The following attributes to configure the "mysql" cookbook behaviour are set by default

``` ruby
default['mysql']['client']['version'] = '5.6'
default['mysql']['version'] = '5.6'
default['mysql']['server_root_password'] = 'dXVC5CyvJzTF3VFCUA8pYntDC5BSG2'
default['mysql']['allow_remote_root'] = false
default['mysql']['remove_anonymous_users'] = true

default['kaltiot_mysql']['deployServer'] = true
```

Recipes
-------
This cookbook provides only the following recipe:

- `default.rb` - *Use this recipe* to install mysql client and the server in case the attribute node['kaltiot_mysql']['deployServer'] is set to true.

License & Authors
-----------------
- Author:: Enrique Cordero (<enrique.cordero@kaltiot.com>)

Copyright 2014-2015, Kaltio Technologies Oy

All Rights Reserved