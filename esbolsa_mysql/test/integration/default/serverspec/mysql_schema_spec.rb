require 'serverspec'

set :backend, :exec

##########################################################################
# TEST THE DATABASE CREATION AND USER
##########################################################################

describe command("echo 'SHOW SCHEMAS;' | /usr/bin/mysql -u root -h 127.0.0.1 -P 3306 -pSHibe100p! | grep spikemc_app_gestion_capital") do
  its(:exit_status) { should eq 0 }
end

describe command("echo 'SHOW SCHEMAS;' | /usr/bin/mysql -u root -h 127.0.0.1 -P 3306 -pSHibe100p! | grep otherdb") do
  its(:exit_status) { should eq 1 }
end

def mysql_user_cmd
  <<-EOF
  /usr/bin/mysql \
  -h 127.0.0.1 \
  -P 3306 \
  -u root \
  -pdXVC5CyvJzTF3VFCUA8pYntDC5BSG2 \
  -e "SELECT Host,User,Password FROM mysql.user WHERE User='spikemc_gc' AND Host='localhost';" \
  --skip-column-names
  EOF
end

describe command(mysql_user_cmd) do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match(/| localhost | probesrv | *16B78F73606BDB15F498C05B75043EEE4124D4FD |/) }
end

