require 'serverspec'

set :backend, :exec

def mysql_cmd
  <<-EOF
  /usr/bin/mysql \
  -h 127.0.0.1 \
  -P 3306 \
  -u root \
  -pdXVC5CyvJzTF3VFCUA8pYntDC5BSG2 \
  -e "SELECT Host,User,Password FROM mysql.user WHERE User='root' AND Host='localhost';" \
  --skip-column-names
  EOF
end

describe command(mysql_cmd) do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match(/| localhost | root | *C2F0E5A10408BBECAA2C5F11419885EB3070F1C1 |/) }
end

describe command("service mysqld status") do
  its(:exit_status) { should eq 0 }
  #its(:stdout) { should match(/active/) }  <- For Centos7
  its(:stdout) { should match(/running.../) }
end

describe command("mysql -h localhost -V") do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match(/Ver 14.14 Distrib 5.6.22/) }
end