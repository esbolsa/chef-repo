#
# Cookbook Name:: esbolsa_mysql
# Recipe:: default
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

include_recipe 'esbolsa_mysql::mysql_installation'

include_recipe 'esbolsa_mysql::mysql_schema'
