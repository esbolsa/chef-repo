#
# Cookbook Name:: kaltiot_mysql_own
# Recipe:: mysql_instalation
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

remote_file "#{Chef::Config[:file_cache_path]}/mysql-community-release-el6-5.noarch.rpm" do
    source "http://dev.mysql.com/get/mysql-community-release-el6-5.noarch.rpm"
    action :create
end

rpm_package "mysql-community-release" do
    source "#{Chef::Config[:file_cache_path]}/mysql-community-release-el6-5.noarch.rpm"
    action :install
end

package "mysql-community-client" do
	action :install
end

package "mysql-community-server" do
	action :install
end

package "mysql-community-devel" do
	action :install
end

execute "chkconfig mysqld on" do
  command "chkconfig mysqld on"
end

execute "service mysqld start" do
  command "service mysqld start"
end

execute "mysqladmin -u root password NEWPASSWORD" do
  command "mysqladmin -u root password #{node['esbolsa_mysql']['server_root_password']}"
end

template "/etc/my.cnf" do
  source "my.cnf.erb"
  mode "0644"
end

execute "service mysqld restart" do
  command "service mysqld restart"
end
