#
# Cookbook Name:: kaltiot_mysql_own
# Recipe:: mysql_schema
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

mysql_dump_plus = Chef::Config[:file_cache_path] + "/plus_schema.sql"
mysql_dump_cursos = Chef::Config[:file_cache_path] + "/moodle_schema.sql"
mysql_dump_foro = Chef::Config[:file_cache_path] + "/smf_schema.sql"
mysql_dump_blog = Chef::Config[:file_cache_path] + "/wp_schema.sql"

# Dump plus database
cookbook_file mysql_dump_plus do
  source "plus_schema.sql"
  mode "0744"
end

execute "Import database schema" do
  command "mysql -u root -p#{node['esbolsa_mysql']['server_root_password']} < #{mysql_dump_plus}"
end


# Dump cursos database
cookbook_file mysql_dump_cursos do
  source "moodle_schema.sql"
  mode "0744"
end

execute "Import database schema" do
  command "mysql -u root -p#{node['esbolsa_mysql']['server_root_password']} < #{mysql_dump_cursos}"
end


# Dump foro database
cookbook_file mysql_dump_foro do
  source "smf_schema.sql"
  mode "0744"
end

execute "Import database schema" do
  command "mysql -u root -p#{node['esbolsa_mysql']['server_root_password']} < #{mysql_dump_foro}"
end


# Dump blog database
cookbook_file mysql_dump_blog do
  source "wp_schema.sql"
  mode "0744"
end

execute "Import database schema" do
  command "mysql -u root -p#{node['esbolsa_mysql']['server_root_password']} < #{mysql_dump_blog}"
end
