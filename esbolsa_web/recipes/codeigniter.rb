#
# Cookbook Name:: esbolsa_web
# Recipe:: codeigniter
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

installation_path = node['esbolsa_web']['nginx']['root_path']
nodeuser = node['esbolsa_web']['nginx']['user'] 
codeigniter_final_dir = "#{installation_path}/../codeigniter"
esbolsa_zip = "esbolsa-#{node['esbolsa_web']['codeigniter']['release']}.zip"


# -----------------------------------------------------------------
#  Delete previous installation if exists
# -----------------------------------------------------------------

directory codeigniter_final_dir do
  action :delete
  recursive true
end

directory "#{installation_path}/img" do
  action :delete
  recursive true
end

directory "#{installation_path}/css" do
  action :delete
  recursive true
end

directory "#{installation_path}/fonts" do
  action :delete
  recursive true
end

directory "#{installation_path}/js" do
  action :delete
  recursive true
end

directory "#{installation_path}/lib" do
  action :delete
  recursive true
end

# -----------------------------------------------------------------
#  Set proper permissions
# -----------------------------------------------------------------

directory "#{installation_path}/.." do
  owner nodeuser
  group nodeuser
  mode "0755"
  action :create
end

directory "#{installation_path}" do
  owner nodeuser
  group nodeuser
  mode "0755"
  action :create
end

# -----------------------------------------------------------------
#  Create needed directories if they do not exist
# -----------------------------------------------------------------

directory "#{installation_path}/cache" do
  owner nodeuser
  group nodeuser
  mode "0755"
  action :create
  recursive true
end


# -------------------------------------------------------
# Unzip Codeigniter code
# -------------------------------------------------------
esbolsa = Chef::Config[:file_cache_path] + "/#{esbolsa_zip}"

cookbook_file esbolsa do
  source "#{esbolsa_zip}"
  mode "0744"
end

package "unzip" do
  action :install
end

execute "unzip esbolsa Web site" do
	cwd "#{installation_path}/.."
	user nodeuser 
	group nodeuser
	action :run   
  	command "unzip #{esbolsa}"
end

# -------------------------------------------------------------
# Create the index.php for codeIgniter
# -------------------------------------------------------------

template "#{installation_path}/index.php" do
  	source "codeigniter/index.php.erb"
  	mode 0644
	user nodeuser 
	group nodeuser
  	variables(
	    :environment   		=> node['esbolsa_web']['codeigniter']['environment'])
end
