#
# Cookbook Name:: esbolsa_web
# Recipe:: varnish
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

include_recipe 'varnish'

template "#{node['varnish']['dir']}/#{node['varnish']['vcl_conf']}" do
  source "varnish/default.vcl.erb"
  cookbook "esbolsa_web"
  mode 0644
  variables(
    :server_port   => node['esbolsa_web']['nginx']['server_port'])
end

service "varnish" do
  action [ :restart ]
end	