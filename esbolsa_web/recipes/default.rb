#
# Cookbook Name:: esbolsa_web
# Recipe:: default
#
# Copyright (c) 2015 esBolsa, All Rights Reserved.

include_recipe 'esbolsa_web::server_preparation'

include_recipe 'esbolsa_web::motd'

include_recipe 'esbolsa_web::usergroup'

include_recipe 'esbolsa_web::php_fpm'

include_recipe 'esbolsa_web::nginx'

include_recipe 'esbolsa_web::varnish'

include_recipe 'esbolsa_web::codeigniter'
