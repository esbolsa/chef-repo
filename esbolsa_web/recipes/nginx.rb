#
# Cookbook Name:: esbolsa_web
# Recipe:: nginx
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

# install nginx web server

include_recipe 'nginx'

# nginx configuration
template "#{node['nginx']['dir']}/nginx.conf" do
  source "nginx/nginx.conf.erb"
  mode 0644
  variables(
    :user        		=> node['esbolsa_web']['nginx']['user'],
    :worker_processes   => node['esbolsa_web']['nginx']['worker_processes'],
    :worker_connections => node['esbolsa_web']['nginx']['worker_connections'],
    :keepalive_timeout  => node['esbolsa_web']['nginx']['keepalive_timeout'],
    :server_port        => node['esbolsa_web']['nginx']['server_port'],
    :root_path			=> node['esbolsa_web']['nginx']['root_path'])
end

service "nginx" do
  action [ :restart ]
end