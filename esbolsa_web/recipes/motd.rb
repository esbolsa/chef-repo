#
# Cookbook Name:: esbolsa_web
# Recipe:: motd
#
# Copyright (c) 2015 esBolsa, All Rights Reserved.

date = Time.now.strftime("%m/%d/%Y")

if node['esbolsa_web']['os'] == 'amazon'
	template "/etc/custom-motd" do
		source "motd/amazon-motd.erb"
		mode "0644"
		variables(
			:lastrelease => node['esbolsa_web']['motd']['last_release'],
			:servercreated => date,
			:stack => node[:opsworks][:stack],
	    	:layers => node[:opsworks][:layers],
	    	:instance => node[:opsworks][:instance]
		)
	end
else
	template "/etc/custom-motd" do
	source "motd/default-motd.erb"
	mode "0644"
	variables(
		:lastrelease => node['esbolsa_web']['motd']['last_release'],
		:servercreated => date
	)
	end
end

link "/etc/motd" do
  to "/etc/custom-motd"
end