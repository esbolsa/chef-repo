#
# Cookbook Name:: esbolsa_web
# Recipe:: wordpress
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.


# -----------------------------------------------------------------
#  Create the path where Wordpress will be installed
# -----------------------------------------------------------------
wordpress_path = node['esbolsa_web']['nginx']['root_path']

directory wordpress_path do
  owner node['esbolsa_web']['nginx']['user']
  group node['esbolsa_web']['nginx']['user'] 
  mode "0755"
  action :create
  recursive true
end

# -----------------------------------------------------------------
#  Download latest Wordpress and untar it in the directory
# -----------------------------------------------------------------
wordpress_latest = Chef::Config[:file_cache_path] + "/wordpress-latest.tar.gz"

remote_file wordpress_latest do
  source "http://wordpress.org/latest.tar.gz"
  mode "0644"
end

execute "untar-wordpress" do
  cwd node['esbolsa_web']['nginx']['root_path']
  command "tar --strip-components 1 -xzf " + wordpress_latest
end