#
# Cookbook Name:: esbolsa_web
# Recipe:: usergroup
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

group node['esbolsa_web']['nginx']['user'] do
  action :create
end

user node['esbolsa_web']['nginx']['user'] do
  gid node['esbolsa_web']['nginx']['user']
  system true
  shell "/bin/false"
  action :create
end

user node['esbolsa_web']['nginx']['user'] do
  action :lock
end
