#
# Cookbook Name:: esbolsa_web
# Recipe:: server_preparation
#
# Copyright (c) 2015 esBolsa, All Rights Reserved.

# ------------------------------------------------------
# Update the server to make sure that we are working
# with the latest packages
# ------------------------------------------------------
execute "apt-update" do
  command "apt-get -y update"
  action :nothing
end

execute "apt-upgrade" do
  command "apt-get -y upgrade"
  action :nothing
end

# -------------------------------------------------------
# Fixing problem with locale that prevents things to 
# be installed
# -------------------------------------------------------
execute "locale-gen" do
  command "locale-gen fi_FI.UTF-8"
  action :nothing
end


execute "reconfigure locales" do
  command "dpkg-reconfigure locales"
  action :nothing
end

# -------------------------------------------------------
# Install vim
# -------------------------------------------------------
package 'vim'
