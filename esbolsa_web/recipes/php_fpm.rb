#
# Cookbook Name:: esbolsa_web
# Recipe:: php_fpm
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

node.default['php_fpm']['install_php_modules'] = true
node.default['php_fpm']['php_modules'] = ['php5-common','php5-mysql','php5-curl','php5-gd']

include_recipe "php5-fpm::install"

# nginx configuration
template "/etc/php5/fpm/pool.d/www.conf" do
  source "php5-fpm/www.conf.erb"
  mode 0644
  variables(
    :user        				=> node['esbolsa_web']['php-fpm']['user'],
    :group        				=> node['esbolsa_web']['php-fpm']['group'],
    :listen_socket 				=> node['esbolsa_web']['php-fpm']['listen_socket'],
    :listen_owner 				=> node['esbolsa_web']['php-fpm']['listen_owner'],
    :listen_group  				=> node['esbolsa_web']['php-fpm']['listen_group'],
    :pm_max_children			=> node['esbolsa_web']['php-fpm']['pm_max_children'],
    :pm_start_servers			=> node['esbolsa_web']['php-fpm']['pm_start_servers'],
    :pm_min_spare_servers		=> node['esbolsa_web']['php-fpm']['pm_min_spare_servers'],
    :pm_max_spare_servers		=> node['esbolsa_web']['php-fpm']['pm_max_spare_servers'],
    :pm_process_idle_timeout	=> node['esbolsa_web']['php-fpm']['pm_process_idle_timeout'])
end

service "php5-fpm" do
  action [ :restart ]
end