default['esbolsa_web']['os'] = 'centos'

default['esbolsa_web']['motd']['last_release'] = '1.0.0'

default['esbolsa_web']['php-fpm']['user'] = 'www-data'
default['esbolsa_web']['php-fpm']['group'] = 'www-data'
default['esbolsa_web']['php-fpm']['listen_socket'] = '/var/run/php5-fpm.sock'
default['esbolsa_web']['php-fpm']['listen_owner'] = 'www-data'
default['esbolsa_web']['php-fpm']['listen_group'] = 'www-data'
default['esbolsa_web']['php-fpm']['pm_max_children'] = 4
default['esbolsa_web']['php-fpm']['pm_start_servers'] = 3
default['esbolsa_web']['php-fpm']['pm_min_spare_servers'] = 2
default['esbolsa_web']['php-fpm']['pm_max_spare_servers'] = 3
default['esbolsa_web']['php-fpm']['pm_process_idle_timeout'] = '30s'

default['esbolsa_web']['nginx']['server_port'] = 8080
default['esbolsa_web']['nginx']['user'] = 'www-data'
default['esbolsa_web']['nginx']['worker_processes'] = 1
default['esbolsa_web']['nginx']['worker_connections'] = 256
default['esbolsa_web']['nginx']['keepalive_timeout'] = 30
default['esbolsa_web']['nginx']['root_path'] = '/var/www/html'

default['varnish']['listen_port'] = '80'

default['esbolsa_web']['codeigniter']['environment'] = 'production'
default['esbolsa_web']['codeigniter']['release'] = '3.5.0'
