#
# Cookbook Name:: esbolsa_redis
# Recipe:: redis_populate
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

# ----------------------------------------------------------------
#  This should be proper way of doing it but Redis does not 
#  like such a big files
# ----------------------------------------------------------------
#advanceDecline = Chef::Config[:file_cache_path] + "/ad"
#
#cookbook_file advanceDecline do
#  source "ad"
#  mode "0744"
#end
#
#execute "populate AD" do
#    command "cat #{advanceDecline} | redis-cli --pipe"
#end


jar = Chef::Config[:file_cache_path] + "/RedisPutData.jar"

cookbook_file jar do
  source "jar/RedisPutData.jar"
  mode 0775
end

execute "Put data Redis" do
  command "java -Dfile.encoding=UTF-8 -jar #{jar}"
  action :run
end

execute 'remove java 7' do
  command 'sudo yum remove java-1.7.0-openjdk --assumeyes'
end
execute 'install java 8' do
  command 'sudo yum install java-1.8.0 --assumeyes'
end
