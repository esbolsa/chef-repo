#
# Cookbook Name:: kaltiot_redis
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.


include_recipe 'java'

include_recipe 'redisio::default'

include_recipe 'redisio::enable'

include_recipe 'esbolsa_redis::redis_populate'
