name             'esbolsa_redis'
maintainer       'Enrique Cordero'
maintainer_email 'enrique.cordero@esbolsa.com'
license          'all_rights'
description      'Installs/Configures Redis'
long_description 'Installs/Configures Redis'
version          '1.0.0'

depends "java",     "~> 1.31.0"
depends 'redisio', '~> 2.2.4'
