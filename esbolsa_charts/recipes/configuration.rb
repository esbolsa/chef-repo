#
# Cookbook Name:: esbolsa_charts
# Recipe:: configuration
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

template "/opt/esbolsa-charts/conf/esbolsa-charts.properties" do
	source "esbolsa-charts.properties.erb"
    owner "esbolsa"
    group "esbolsa" 
	mode "0664"
	variables(
		:server_domain => node['esbolsa_charts']['server_domain'],
		:loglevel => node['esbolsa_charts']['loglevel'],
		:redis_server => node['esbolsa_charts']['redis_server'],
		:mysql_server => node['esbolsa_charts']['mysql_server'],
		:mysql_dbname => node['esbolsa_charts']['mysql_dbname'],
		:mysql_user => node['esbolsa_charts']['mysql_user'],
		:mysql_pwd => node['esbolsa_charts']['mysql_pwd']
	)
end
