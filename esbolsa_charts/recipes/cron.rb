#
# Cookbook Name:: esbolsa_charts
# Recipe:: cron
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

cron "empty historical data" do
  minute '0'
  hour '0'
  user 'esbolsa'
  command 'rm -f /opt/esbolsa-charts/data/historical/*:*.csv 2>/dev/null'
  action :create
end

cron "empty historical data 2" do
  minute '10'
  hour '0'
  user 'esbolsa'
  command 'rm -f /opt/esbolsa-charts/data/historical/*.csv 2>/dev/null'
  action :create
end

cron "daily_stock_data_update" do
  minute '0'
  hour '3'
  user 'esbolsa'
  command '/opt/esbolsa-charts/scripts/esbolsa-charts.sh'
  action :create
end