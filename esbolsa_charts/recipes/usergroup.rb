#
# Cookbook Name:: esbolsa_charts
# Recipe:: usergroup
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

nodeuser = 'esbolsa'
nodegroup = 'esbolsa'
user_home = '/home/esbolsa'

group nodegroup do
    action :create
end

user nodeuser do
    supports :manage_home => true
    shell "/bin/bash"
    home user_home
    gid nodegroup
    system true
    action :create
end

execute "remove password" do
    command "sudo echo '#{nodeuser}  ALL = (ALL) NOPASSWD: ALL' >> /etc/sudoers"
end
