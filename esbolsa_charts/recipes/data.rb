#
# Cookbook Name:: esbolsa_charts
# Recipe:: data
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

cookbook_file "/opt/esbolsa-charts/data/original/bonds.txt" do
  source "data/bonds.txt"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

cookbook_file "/opt/esbolsa-charts/data/original/advanceDecline.txt" do
  source "data/advanceDecline.txt"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

cookbook_file "/opt/esbolsa-charts/data/original/americanSectors.txt" do
  source "data/americanSectors.txt"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

cookbook_file "/opt/esbolsa-charts/data/original/worldIndexes.txt" do
  source "data/worldIndexes.txt"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

cookbook_file "/opt/esbolsa-charts/data/original/stocks.txt" do
  source "data/stocks.txt"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

cookbook_file "/opt/esbolsa-charts/data/original/europeanSectors.txt" do
  source "data/europeanSectors.txt"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

cookbook_file "/opt/esbolsa-charts/data/original/commodities.txt" do
  source "data/commodities.txt"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

cookbook_file "/opt/esbolsa-charts/data/original/currencies.txt" do
  source "data/currencies.txt"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

cookbook_file "/opt/esbolsa-charts/data/original/CME-FGBL2.csv" do
  source "data/CME-FGBL2.csv"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end