#
# Cookbook Name:: esbolsa_charts
# Recipe:: scripts
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

# Script to start the java application

template "/opt/esbolsa-charts/scripts/esbolsa-charts.sh" do
	source "esbolsa-charts.sh.erb"
    owner "esbolsa"
    group "esbolsa" 
	mode "0775"
	variables(
		:minmem => node['esbolsa_charts']['jvm_min_memory'],
		:maxmem => node['esbolsa_charts']['jvm_max_memory']
	)
end