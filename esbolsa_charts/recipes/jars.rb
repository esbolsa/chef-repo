#
# Cookbook Name:: esbolsa_charts
# Recipe:: jars
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

jar_name = "charts-#{node['esbolsa_charts']['release']}-jar-with-dependencies.jar"

cookbook_file "/opt/esbolsa-charts/jars/#{jar_name}" do
  source "jars/#{jar_name}"
  owner "esbolsa"
  group "esbolsa" 
  mode "0644"
end

link "/opt/esbolsa-charts/jars/ChartsWithDependencies.jar" do
  to "/opt/esbolsa-charts/jars/#{jar_name}"
end


execute 'remove java 7' do
  command 'sudo yum remove java-1.7.0-openjdk --assumeyes'
end
execute 'install java 8' do
  command 'sudo yum install java-1.8.0 --assumeyes'
end
