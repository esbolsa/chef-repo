#
# Cookbook Name:: esbolsa_charts
# Recipe:: mysql_schema
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

mysql_dump_plus = Chef::Config[:file_cache_path] + "/plus_schema.sql"

# Dump plus database
cookbook_file mysql_dump_plus do
  source "mysql/plus_schema.sql"
  mode "0744"
end

execute "Import database schema" do
  command "mysql -u root -p#{node['esbolsa_charts']['mysql_root_pwd']} < #{mysql_dump_plus}"
end
