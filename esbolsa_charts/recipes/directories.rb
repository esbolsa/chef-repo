#
# Cookbook Name:: esbolsa_charts
# Recipe:: maindirectory
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

directory '/opt/esbolsa-charts' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0750"
  action :create
end

directory '/opt/esbolsa-charts/conf' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0775"
  action :create
end

directory '/opt/esbolsa-charts/conf/production' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0775"
  action :create
end

directory '/opt/esbolsa-charts/data' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0775"
  action :create
end

directory '/opt/esbolsa-charts/data/dividend' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0775"
  action :create
end

directory '/opt/esbolsa-charts/data/historical' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0775"
  action :create
end

directory '/opt/esbolsa-charts/data/original' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0775"
  action :create
end

directory '/opt/esbolsa-charts/data/currency' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0775"
  action :create
end

directory '/opt/esbolsa-charts/jars' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0755"
  action :create
end

directory '/opt/esbolsa-charts/logs' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0770"
  action :create
end

directory '/opt/esbolsa-charts/scripts' do
  owner "esbolsa"
  group "esbolsa" 
  mode "0775"
  action :create
end

