#
# Cookbook Name:: esbolsa_charts
# Recipe:: default
#
# Copyright (c) 2015 Enrique Cordero, All Rights Reserved.

include_recipe 'esbolsa_charts::motd'

include_recipe 'esbolsa_charts::usergroup'

include_recipe 'esbolsa_charts::mysql_installation'

include_recipe 'esbolsa_charts::mysql_schema'

include_recipe 'esbolsa_charts::directories'

include_recipe 'esbolsa_charts::configuration'

include_recipe 'esbolsa_charts::data'

include_recipe 'esbolsa_charts::jars'

include_recipe 'esbolsa_charts::scripts'

include_recipe 'esbolsa_charts::cron'

include_recipe 'redisio::default'

include_recipe 'redisio::enable'