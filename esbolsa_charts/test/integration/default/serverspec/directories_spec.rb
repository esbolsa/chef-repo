require 'serverspec'

set :backend, :exec

describe file('/opt/esbolsa-charts') do
  it 'esbolsa-charts main directory exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 750
  end
end

describe file('/opt/esbolsa-charts/conf') do
  it 'esbolsa-charts conf exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 775
  end
end

describe file('/opt/esbolsa-charts/conf/production') do
  it 'esbolsa-charts conf exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 775
  end
end

describe file('/opt/esbolsa-charts/data') do
  it 'esbolsa-charts data exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 775
  end
end

describe file('/opt/esbolsa-charts/data/historical') do
  it 'esbolsa-charts data exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 775
  end
end

describe file('/opt/esbolsa-charts/data/original') do
  it 'esbolsa-charts data exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 775
  end
end

describe file('/opt/esbolsa-charts/data/dividend') do
  it 'esbolsa-charts data exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 775
  end
end

describe file('/opt/esbolsa-charts/jars') do
  it 'esbolsa-charts jars exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 755
  end
end

describe file('/opt/esbolsa-charts/logs') do
  it 'esbolsa-charts logs exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 770
  end
end

describe file('/opt/esbolsa-charts/scripts') do
  it 'esbolsa-charts scripts exists and is a directory' do
    should be_directory
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 775
  end
end