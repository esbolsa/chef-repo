require 'serverspec'

set :backend, :exec

describe file('/opt/esbolsa-charts/data/original/stocks.csv') do
  it 'Stocks cvs file exists' do
    should be_file
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 644
  end

  its(:sha256sum) {
    should eq '43725be20eaf8637686b095bea900e95691dc8dad5bf377d1570ed1af017ec60'
  }  
end


describe file('/opt/esbolsa-charts/data/original/previousRedisTickers.csv') do
  it 'Previous Redis Tickers file exists' do
    should be_file
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 644
  end

  its(:sha256sum) {
    should eq '58f90ea1d9de84160c9907a241b1132c72cfd7a52d6098200f5f5b8f94460839'
  }  
end

