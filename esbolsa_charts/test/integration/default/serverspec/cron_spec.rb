require 'serverspec'

set :backend, :exec

describe cron do
  it { should have_entry('0 1 * * * /opt/esbolsa-charts/scripts/esbolsa-charts.sh').with_user('esbolsa') }
end