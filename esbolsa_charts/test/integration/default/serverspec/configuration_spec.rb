require 'serverspec'

set :backend, :exec

describe file('/opt/esbolsa-charts/conf/esbolsa-charts.properties') do
  it 'esbolsa-charts.properties file exists and is a file' do
    should be_file
    should be_readable
  end
end


# This test is prepared to check the local files. We should be aple to check other environments
# TODO: Automate this at some point, not urgent
# http://www.hurryupandwait.io/blog/accessing-chef-node-attributes-from-kitchen-tests

describe file('/opt/esbolsa-charts/conf/esbolsa-charts.properties') do
  it 'esbolsa-charts.properties file contains what was expected' do
    should contain 'log4j.rootLogger=DEBUG, errorOutput, normalOutput'
    should contain 'server.redis.host = 192.168.33.15'
  end
end

