require 'serverspec'

set :backend, :exec

describe file('/opt/esbolsa-charts/jars/charts-1.0.0-jar-with-dependencies.jar') do
  it 'esBolsa-charts jar file exists' do
    should be_file
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 644
  end

  #its(:sha256sum) {
  #  should eq '4d26eb0d05c2d1874bf59cb9c0d829652f51551d445497cf4420af4fd4bd5cc7'
  #}  
end

describe file('/opt/esbolsa-charts/jars/ChartsWithDependencies.jar') do
  it 'ChartsWithDependencies symlink exists points to charts-1.0.0-jar-with-dependencies.jar' do
    should be_symlink
    should be_linked_to '/opt/esbolsa-charts/jars/charts-1.0.0-jar-with-dependencies.jar'
  end
end
