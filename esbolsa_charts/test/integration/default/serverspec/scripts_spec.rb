require 'serverspec'

set :backend, :exec

describe file('/opt/esbolsa-charts/scripts/esbolsa-charts.sh') do
  it 'esbolsa-charts.sh script file exists' do
    should be_file
    should be_owned_by 'esbolsa'
    should be_grouped_into 'esbolsa'
    should be_mode 775
  end
end

describe file('/opt/esbolsa-charts/scripts/esbolsa-charts.sh') do
  it 'esbolsa-charts.sh file contains what was expected' do
    should contain 'XMS=\"256M\"'
    should contain 'XMX=\"400M\"'
  end
end
