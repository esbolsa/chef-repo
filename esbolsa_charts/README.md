EsBolsa Chart Cookbook
==============

Installs EsBolsa Chart server and configures it.


Requirements
------------
### Cookbooks
This cookbook does not depend directly on other cookbooks, but the following cookbook installatios are required for proper server behaviour: 
- kaltiot_java
- kaltiot_redis

### Platforms
This cookbook has been tested for 

- Centos 6.6
- Amazon Linux (Latest release)

Attributes
----------
The following attributes to configure the "java" cookbook behaviour are set by default

``` ruby
default['esbolsa_charts']['loglevel'] = 'DEBUG'
default['esbolsa_charts']['redis_server'] = 'localhost'
```

- `loglevel` - Log level for the application server
- `redis_server` - Redis host address


Recipes
-------
This cookbook provides the following main recipe:

- `default.rb` - *Use this recipe* to create a EsBolsa Chart server.

Apart from those two, there are some other recipes that are used by the two above, but which should not be directly used:

- `configuration.rb` - Creates the charts server configuration file
- `cron.rb` - Configures java process to run automatically daily
- `data.rb` - Copies the stocks database as a csv
- `directories.rb` - Creates the directories used by the charts server
- `jars.rb` - Installs the charts server jar file
- `motd.rb` - Changes welcome message from the server
- `scripts.rb` - Installs the scripts used to run the charts server
- `usergroup.rb` - Create the user and group that is used to run the charts server.


Change log
-----------------

### Release 1.0.0

- Initial release. 

### Release 1.0.1

- Added cron and bug fixes

### Release 1.0.2

- Added motd
- Added redis server
- Removed file related to previous redis situation (no longer used)
- Remove scripts from files (now templates)



License & Authors
-----------------
- Author:: Enrique Cordero (<enrique.cordero@outlook.com>)

Copyright 2014-2015, esBolsa 

All Rights Reserved