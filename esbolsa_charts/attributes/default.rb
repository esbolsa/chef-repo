# This values are default values for Local environment.
# Those can be modified using kitchen.yml if needed
#
# Allowed environments:
#  - Local
#  - Production
#

default['esbolsa_charts']['os'] = 'centos'
default['esbolsa_charts']['release'] = '2.2.4'

default['esbolsa_charts']['loglevel'] = 'DEBUG'
default['esbolsa_charts']['redis_server'] = '192.168.33.15'
default['esbolsa_charts']['server_domain'] = 'http://esbolsa.com/'

default['esbolsa_charts']['jvm_min_memory'] = '256M'
default['esbolsa_charts']['jvm_max_memory'] = '400M'

default['esbolsa_charts']['mysql_server'] = '192.168.33.14'
default['esbolsa_charts']['mysql_user'] = 'esbolsa_plus'
default['esbolsa_charts']['mysql_pwd'] = '3sb0ls42011#plus'
default['esbolsa_charts']['mysql_root_pwd'] = '3sB0ls42011#db'

default['mysql']['version'] = '5.6'
